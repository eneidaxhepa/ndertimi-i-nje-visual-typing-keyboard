﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Media;

namespace Keyboard
{
    public partial class MainWindow : Window
    {
        private string currentText = "";
        private bool capsLockEnabled = false;

        public MainWindow()
        {
            InitializeComponent();
            InitializeKeyboard();
            Loaded += MainWindow_Loaded;
        }
        private string targetText = "Ky eshte nje testim i shpejtesise suaj ne typing. Provoni veten duke plotesuar sakte kete test. Per te shkruar sa me shpejt duhet te praktikoni shkrimin me 10 gishta.";

        private void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {

            Paragraph paragraph = new Paragraph();


            Run initialRun = new Run("Ky eshte nje testim i shpejtesise suaj ne typing. Provoni veten duke plotesuar sakte kete test. Per te shkruar sa me shpejt duhet te praktikoni shkrimin me 10 gishta.");
            paragraph.Inlines.Add(initialRun);


            OutputRichTextBox.Document.Blocks.Clear();
            OutputRichTextBox.Document.Blocks.Add(paragraph);
        }




        private void InitializeKeyboard()
        {
            if (KeyboardGrid != null && KeyboardGrid.Children != null)
            {
                foreach (UIElement element in KeyboardGrid.Children)
                {
                    if (element is Button button)
                    {
                        string buttonText = button.Content.ToString();
                        if (buttonText != "CapsLock") // Ekzkludo rastin e veçantë
                        {
                            button.Click += Button_Click;
                        }
                    }
                }
            }
        }





        private List<int> foundIndexes = new List<int>();


        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Button clickedButton = (Button)sender;
            string buttonText = clickedButton.Content.ToString();


            if (currentText == targetText)
            {
                return;
            }


            if (buttonText == "CapsLock")
            {

            }
            else if (buttonText == "Enter")
            {

            }
            else if (buttonText == "Space")
            {

            }
            else
            {

                if (currentText.Length < targetText.Length && targetText[currentText.Length] == buttonText[0])
                {

                    foundIndexes.Add(currentText.Length);
                }
                else
                {
                    return;
                }
            }


            currentText += capsLockEnabled ? buttonText.ToUpper() : buttonText.ToLower();
            UpdateOutputTextBox();
        }






        private void Shkronje_Click(object sender, RoutedEventArgs e)
        {
            Button clickedButton = (Button)sender;
            string buttonText = clickedButton.Content.ToString();

            if (buttonText == "CapsLock")
            {
                capsLockEnabled = !capsLockEnabled;
                Capslock_Click_1();
            }
            else if (buttonText == "Enter")
            {
                currentText += Environment.NewLine;
                UpdateOutputTextBox();
            }
            else if (buttonText == "Space")
            {
                currentText += " ";
                UpdateOutputTextBox();
            }
            else
            {
                currentText += capsLockEnabled ? buttonText.ToUpper() : buttonText.ToLower();
                UpdateOutputTextBox();
            }
        }





        private void Capslock_Click_1()
        {
            throw new NotImplementedException();
        }

        private void UpdateOutputRichTextBox()
        {

            Run run = new Run(currentText);


            OutputRichTextBox.Document.Blocks.Clear();
            OutputRichTextBox.Document.Blocks.Add(new Paragraph(run));

            foreach (int index in foundIndexes)
            {
                if (index < currentText.Length)
                {
                    run = new Run(currentText[index].ToString());
                    run.Foreground = Brushes.Green;
                    TextPointer start = OutputRichTextBox.Document.ContentStart.GetPositionAtOffset(index);
                    if (start != null)
                    {
                        TextRange textRange = new TextRange(start, start.GetPositionAtOffset(1));
                        textRange.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.Green);
                    }
                }
            }
        }





        private void UpdateOutputTextBox()
        {
            OutputRichTextBox.Document.Blocks.Clear();

            Paragraph paragraph = new Paragraph();
            int targetLength = Math.Min(currentText.Length, targetText.Length);

            for (int i = 0; i < targetText.Length; i++)
            {
                SolidColorBrush foreground;

                if (i < targetLength)
                {
                    foreground = (currentText[i] == targetText[i]) ? Brushes.Green : Brushes.Red;
                }
                else
                {
                    foreground = Brushes.Gray;
                }

                Run run = new Run(targetText[i].ToString());
                run.Foreground = foreground;
                paragraph.Inlines.Add(run);
            }

            OutputRichTextBox.Document.Blocks.Add(paragraph);
        }






        private void Shigjete_Click(object sender, RoutedEventArgs e)
        {
            if (currentText.Length > 0)
            {
                currentText = currentText.Remove(currentText.Length - 1);
                UpdateOutputTextBox();
            }
        }



        private void Enter_Click(object sender, RoutedEventArgs e)
        {
            currentText += Environment.NewLine;
            UpdateOutputTextBox();
        }





        private void PointButton_Click(object sender, RoutedEventArgs e)
        {
            currentText += ".";
            UpdateOutputTextBox();
        }



        private void Space_Click(object sender, RoutedEventArgs e)
        {
            currentText += " ";
            UpdateOutputTextBox();
        }




        private void Capslock_Click_1(object sender, RoutedEventArgs e)
        {
            capsLockEnabled = !capsLockEnabled;

            foreach (UIElement element in KeyboardGrid.Children)
            {
                if (element is Button button)
                {
                    string buttonText = button.Content.ToString();
                    button.Content = capsLockEnabled ? buttonText.ToUpper() : buttonText.ToLower();
                }
            }
        }

    }


    internal class KeyboardGrid
    {
        public static IEnumerable<UIElement> Children { get; internal set; }
    }
}